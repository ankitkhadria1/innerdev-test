class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|

      t.string :name
      t.text :description
      t.datetime :expiration_date

      t.timestamps
    end
    add_reference :tasks, :user, foreign_key: true
  end
end
