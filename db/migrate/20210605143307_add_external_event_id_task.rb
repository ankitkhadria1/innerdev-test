class AddExternalEventIdTask < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :external_event_id, :string
  end
end
