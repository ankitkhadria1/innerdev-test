require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TaskManagement
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.active_job.queue_adapter = :delayed_job
    config.autoload_paths << Rails.root.join('lib')
    config.autoload_paths += Dir["#{config.root}/lib", "#{config.root}/lib/**/"]
    config.time_zone = "Kolkata"
    config.active_record.default_timezone = :utc
    config.assets.paths << Rails.root.join("vendor", "assets", "images")

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # config.to_prepare do
    #   # Or to configure mailer layout
    #   Devise::Mailer.layout "devise_mailer" # email.haml or email.erb
    #   Devise::Mailer.helper :device
    # end

  end
end
