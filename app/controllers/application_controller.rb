class ApplicationController < ActionController::Base

    before_action :authenticate_user!

    def index
        tasks = Task.where(user: current_user)
        @progress_count = tasks.where(task_status: :in_progress).count
        @done_count = tasks.where(task_status: :done).count
        @backlog_count = tasks.where(task_status: :backlog).count

        render template: "applications/index"
    end
        
end
