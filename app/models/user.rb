# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  name                   :string           default(""), not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  access_token           :string
#  expires_at             :datetime
#  refresh_token          :string
#

require "google_calender"

class User < ApplicationRecord

  has_many :tasks, dependent: :delete_all

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2]


  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(:email => data["email"]).first
    unless user
      user = User.create(
            name: data["name"] || "User",
            email: data["email"],
            encrypted_password: Devise.friendly_token[0,20],
            password: Devise.friendly_token[0,20]
      )
    end
    user
  end

  def create_tasks_from_google_calender
    delay(attempts: 1).create_tasks(id)
    true
  end

  def create_tasks(id)
    user = User.find(id)
    google_api_client = GoogleCalender.new(user)
    google_api_client.create_tasks
    true
  end


end
