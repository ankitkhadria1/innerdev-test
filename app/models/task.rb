# == Schema Information
#
# Table name: tasks
#
#  id                :bigint           not null, primary key
#  name              :string
#  description       :text
#  expiration_date   :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :bigint
#  task_status       :integer
#  start_date        :datetime
#  external_event_id :string
#

require "google_calender"

class Task < ApplicationRecord

    belongs_to :user

    before_save :set_default_task_status

    after_create :check_task_status, :delayed_send_gcal_event
    before_destroy :delete_google_calender_event


    enum task_status: [:in_progress, :backlog, :done]

    default_scope { order(created_at: :desc) }


    validates :name, presence: true
    validates :expiration_date, presence: true 
    validates :start_date, presence: true 
  
    validates :start_date, not_in_past: true, on: :create
    validates :expiration_date, not_in_past: true, on: :create
    validate :expiration_date_is_after_start_date
    

    def check_task_status
        current_time = Time.now

        delay.change_task_status(self.id) if current_time > self.expiration_date

        one_day_before = self.expiration_date - 1.days
        one_hour_before = self.expiration_date - 1.hour
        # # Run one day before and one hour
        delay(run_at: one_day_before, attempts: 1).send_reminder(id) if self.expiration_date > current_time && self.task_status == 'in_progress'
        delay(run_at: one_hour_before, attempts: 1).send_reminder(id) if self.expiration_date > current_time && self.task_status == 'in_progress'

        # Change task status after expiration time
        delay(run_at: self.expiration_date + 5.minutes, attempts: 1).send_reminder(self.id) if self.expiration_date > current_time && self.task_status == 'in_progress'

        set_task_status_based_on_dates if current_time > self.expiration_date && self.task_status != 'done'
        true
    end
    
    def set_task_status_based_on_dates
        self.task_status = :backlog if expiration_date < Time.now
        self.task_status = :in_progress if task_status.nil?
    end
    
    def change_task_status(id)
        task = Task.find(id)
        task.set_task_status_based_on_dates
        task.save!
        task
    end

    def send_reminder(id)
        task = Task.find(id)
        UserNotifierMailer.delay.send_task_reminder(task)
    end

    def insert_event_to_google_calender(id)
      task = Task.find(id)
      google_api_client = GoogleCalender.new(task.user, task)
      event = google_api_client.insert_event
      task.external_event_id = event.id
      task.save
      true
    end

    def delayed_send_gcal_event
      delay(attempts: 1).insert_event_to_google_calender(id) if self.is_gcal_linked
      true
    end

    def delete_google_calender_event
      google_api_client = GoogleCalender.new(self.user, self)
      google_api_client.delete_event
      true
    end


    def set_default_task_status
      if task_status.nil?
        self.task_status = :in_progress
      end
    end

    private

    def expiration_date_is_after_start_date
      return if expiration_date.blank? || start_date.blank?
      if expiration_date < start_date
        errors.add(:expiration_date, "cannot be before the start date")
      end
    end    

    def is_gcal_linked
      user.access_token.present?
    end

end
