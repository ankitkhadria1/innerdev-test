class ApplicationMailer < ActionMailer::Base
  layout 'devise_mailer'
  helper 'devise'
  default from: ENV['FROM_EMAIL_ADDRESS'],
          return_path: ENV['FROM_EMAIL_ADDRESS']

end
