class UserNotifierMailer < ApplicationMailer
    default :from => ENV['FROM_EMAIL_ADDRESS']
  
    # send a signup email to the user, pass in the user object that   contains the user's email address
    def send_task_reminder(task)
      @task = task
      @user = task.user
      mail( :to => @user.email,
      :subject => "Pending | Your task is about to expire #{@task.name}" )
    end
end