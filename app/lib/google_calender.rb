require "google/apis/calendar_v3"
require "google/api_client/client_secrets.rb"

class GoogleCalender

    attr_accessor :task
    attr_accessor :user
    attr_accessor :google_api_client

    def initialize(user, task = nil)
        self.user = user
        self.task = task
        self.init_client
    end

    def init_client
        client = Google::Apis::CalendarV3::CalendarService.new
        return unless (self.user.present? && self.user.access_token.present? && self.user.refresh_token.present?)
        secrets = Google::APIClient::ClientSecrets.new({
          "web" => {
            "access_token" => self.user.access_token,
            "refresh_token" => self.user.refresh_token,
            "client_id" => ENV["GOOGLE_OAUTH_CLIENT_ID"],
            "client_secret" => ENV["GOOGLE_OAUTH_CLIENT_SECRET"]
          }
        })
        begin
          client.authorization = secrets.to_authorization
          client.authorization.grant_type = "refresh_token"
    
          if !self.user.present?
            client.authorization.refresh!
            self.user.update_attributes(
              access_token: client.authorization.access_token,
              refresh_token: client.authorization.refresh_token,
              expires_at: client.authorization.expires_at.to_i
            )
          end
        rescue => e
          puts "Error in authorization"
        end
        self.google_api_client = client
    end

    def create_event
        event = Google::Apis::CalendarV3::Event.new(
          summary: self.task.name,
          location: '',
          description: self.task.description,
          start: Google::Apis::CalendarV3::EventDateTime.new(
            date_time: self.task.start_date.to_datetime.rfc3339,
            time_zone: 'Asia/Kolkata'
          ),
          end: Google::Apis::CalendarV3::EventDateTime.new(
            date_time: self.task.expiration_date.to_datetime.rfc3339,
            time_zone: 'Asia/Kolkata'
          ),
          recurrence: [
            'RRULE:FREQ=DAILY;COUNT=2'
          ],
          attendees: [
            Google::Apis::CalendarV3::EventAttendee.new(
              email: self.task.user.email
            )
          ],
          reminders: Google::Apis::CalendarV3::Event::Reminders.new(
            use_default: false,
            overrides: [
              Google::Apis::CalendarV3::EventReminder.new(
                reminder_method: 'email',
                minutes: 24 * 60
              ),
              Google::Apis::CalendarV3::EventReminder.new(
                reminder_method: 'popup',
                minutes: 60
              )
            ]
          )
        )
        event
    end

    def insert_event
      event = create_event
      data = self.google_api_client.insert_event("primary", event)
      data
    end

    def delete_event
        external_event_id = self.task.external_event_id
        self.google_api_client.delete_event('primary', external_event_id)
        true
    end
    
    def create_tasks
        events = self.google_api_client.list_events("primary",
          max_results:   20,
          single_events: true,
          order_by: "startTime",
          time_min: (DateTime.now - 10.days).to_datetime.rfc3339)
        items = events.items
        items.each do |event|
          start_date = event.start.date_time || event.start.date
          event_id = event.id
          end_date = event.end.date_time || event.end.date
          summary = event.summary
          description = event.description
          status = event.status
    
          tasks = Task.where(external_event_id: event_id)
          unless tasks.present?
            task = Task.new
            task.name = summary
            task.external_event_id = event_id
            task.description = description
            task.start_date = start_date
            task.expiration_date = end_date
            task.user = self.user
            if status == "Confirmed"
              task.task_status = :in_progress
            elsif status == "Cancelled"
              task.task_status = :backlog
            else
              task.task_status = :done
            end
            task.save
          end
        end
        true
    end
end